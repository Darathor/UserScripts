# Scripts de personnalisation de sites

Ces styles et scripts permettent de personnaliser le rendu de certains sites. Ces styles et scripts sont proposés sous licence [WTFPL](https://fr.wikipedia.org/wiki/WTFPL), ils sont donc utilisables tels quels ou adaptables librement selon vos besoins.

Pour les installer il faut que votre navigateur dispose d'une extension adaptée.

Pour les scripts :
 * sous Firefox : [Greasemonkey](https://addons.mozilla.org/fr/firefox/addon/greasemonkey/)

Pour les styles :
 * sous Firefox : [Stylus](https://addons.mozilla.org/en-US/firefox/addon/styl-us/)
 * sous Opera : [Stylus](https://addons.opera.com/fr/extensions/details/stylus/)

J'utilise ces styles et scripts sur Firefox (et occasionnellement sur Opera) sans les tester particulièrement sur Chrome ou autres. Et même sur Firefox, je ne garantis en aucun cas le fonctionnement, ne serait-ce que parce qu'**ils sont entièrement dépendants du site concerné** et qu'un quelconque changement sur le dit site peut les rendre totalement inopérants d'un moment à l'autre.

## Liste des scripts

### Messageries et réseaux sociaux

#### Mastodon
 * personnalisation du design et de l'ergonomie : [style](https://framagit.org/Darathor/UserScripts/raw/master/mastodon/ergo-perso.user.css)

#### [Twitter](https://twitter.com/)
 * masquage d'éléments indésirables : [style](https://framagit.org/Darathor/UserScripts/raw/master/twitter.com/masquages.user.css)
 * améliorations ergonomiques : [style](https://framagit.org/Darathor/UserScripts/raw/master/twitter.com/ergo-perso.user.css)
 * améliorations cosmétiques : [style](https://framagit.org/Darathor/UserScripts/raw/master/twitter.com/cosmetique.user.css)

#### Slack
 * ajustements visuels : [style](https://framagit.org/Darathor/UserScripts/raw/master/slack/slack-misc.user.css)
 * thème sombre : [style/externe](https://userstyles.org/styles/117475/slack-night-mode-black)

### Jeux vidéos

#### Dokkan Battle

##### [Wikia](https://dbz-dokkanbattle.fandom.com/wiki/Dragon_Ball_Z_Dokkan_Battle_Wikia)
 * ajustements visuels : [style](https://framagit.org/Darathor/UserScripts/raw/master/dbz-dokkanbattle.wikia.com/ergo-perso.user.css)

##### [DBZ Space](https://dbz.space/)
 * ajustements visuels : [style](https://framagit.org/Darathor/UserScripts/raw/master/dbz.space/ergo-perso.user.css)

#### GuildWars 2
 
##### [Le Bus Magique](https://www.lebusmagique.fr/)
 * ajustements visuels et ergonomiques : [style](https://framagit.org/Darathor/UserScripts/raw/master/lebusmagique.fr/ergo-perso.user.css)

### Divers

#### Wallabag
 * ajustements visuels : [style](https://framagit.org/Darathor/UserScripts/raw/master/wallabag/ergo-perso.user.css)

#### [Tric Trac](https://www.trictrac.net/)
 * ajustements visuels : [style](https://framagit.org/Darathor/UserScripts/raw/master/trictrac.net/ergo-perso.user.css)
 
#### [Manga Sanctuary](https://manga-sanctuary.com/)
 * ajustements visuels : [style](https://framagit.org/Darathor/UserScripts/raw/master/manga-sanctuary.com/ergo-perso.user.css)
 
#### [AlloCiné](http://www.allocine.fr/)
 * ajustements visuels et ergonomiques : [style](https://framagit.org/Darathor/UserScripts/raw/master/allocine.fr/perso.user.css)

#### [Rakuten](https://fr.shopping.rakuten.com/)
 * affichage du panier sur deux colonnes : [style](https://framagit.org/Darathor/UserScripts/-/raw/master/rakuten/two-column-cart.user.css)
 * ajustements divers : [style](https://framagit.org/Darathor/UserScripts/-/raw/master/rakuten/ergo-perso.user.css)

#### Jira
 * ajustements visuels : [style](https://framagit.org/Darathor/UserScripts/raw/master/jira/jira-misc.user.css)
