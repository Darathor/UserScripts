// ==UserScript==
// @name     [GitLab] Fix merge request links.
// @version  1
// @grant    none
// @match https://gitlab.omn.proximis.com/*/merge_requests*
// ==/UserScript==

window.addEventListener('load', function () { 
	document.querySelectorAll('.issues-state-filters a').forEach((element) => element.replaceWith(element.cloneNode(true)))
})