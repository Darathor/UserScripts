// ==UserScript==
// @name         [DocOMN] Correction orthographique
// @namespace    https://darathor.com/
// @version      1.0.0
// @description  Ajoute un bouton passant la page en content editable afin d'activer la correction orthographique.
// @author       Darathor < darathor@free.fr >
// @match        *://doc.master.vm/*
// @match        *://doc.omn.proximis.com/*
// @grant        none
// @license      WTFPL
// ==/UserScript==

(function (document) {
  'use strict';
  
  document.querySelector('body').innerHTML+='<button onclick="var elm = document.querySelector(\'body > .container\'); elm.setAttribute(\'contenteditable\', elm.getAttribute(\'contenteditable\') === \'true\' ? \'false\' : \'true\')" title="activer / désactiver la correction orthographique" class="btn btn-xs btn-default" style="position:fixed;bottom:5px;right:5px;z-index: 5001;"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></button>';
})(window.document);